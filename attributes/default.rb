default['vault_tool']['package_action'] = 'install'
default['vault_tool']['package_version'] = nil

default['vault_tool']['config_dir'] = '/etc/vault'
default['vault_tool']['config_file'] = 'vault.json'
default['vault_tool']['config_file_path'] = nil
default['vault_tool']['config'] = {
  'backend' => {
    'file' => {
      'path' => '/var/lib/vault'
    }
  },
  'listener' => {
    'tcp' => {
      'address' => '127.0.0.1:8200',
      'tls_disable' => '1'
    }
  }
}

default['vault_tool']['client_tls'] = true
default['vault_tool']['vault_bin_path'] = '/usr/bin/vault'
