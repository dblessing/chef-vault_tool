#
# Cookbook Name:: vault_tool
# Recipe:: default
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

include_recipe 'runit'
include_recipe 'chef-vault'

node.default['vault_tool']['config_file_path'] =
  "#{node['vault_tool']['config_dir']}/#{node['vault_tool']['config_file']}"

packagecloud_repo 'blessing_io/vault_tool' do
  case node['platform_family']
  when 'debian'
    type 'deb'
  when 'rhel'
    type 'rpm'
  else
    # TODO: Is this the best way to handle this?
    Chef::Log.fatal('Your operating system is not supported at this time')
  end
end

package 'hashicorp_vault' do
  action node['vault_tool']['package_action']
  version node['vault_tool']['package_version']
  notifies :restart, 'runit_service[vault]'
end

directory node['vault_tool']['config_dir'] do
  mode '0750'
  owner 'root'
  group 'root'
end

template node['vault_tool']['config_file_path'] do
  source 'config.json.erb'
  mode '0640'
  owner 'root'
  group 'root'
  notifies :restart, 'runit_service[vault]'
end

if node['vault_tool']['config']['backend']['file']
  directory node['vault_tool']['config']['backend']['file']['path']
end

runit_service 'vault' do
  default_logger true
  notifies :unseal, 'vault_tool[test]'
end

vault_tool 'test' do
  action [:init, :unseal]
  tls false
  admins 'client1'
  search '*:*'
  clients '*:*'
end
