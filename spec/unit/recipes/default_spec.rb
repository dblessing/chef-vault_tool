#
# Cookbook Name:: vault_tool
# Spec:: default
#
# Copyright (c) 2015 Drew Blessing, All Rights Reserved.

require 'spec_helper'

describe 'vault_tool::default' do
  context 'centos with default attributes' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new(platform: 'centos', version: '6.5')
        .converge(described_recipe)
    end
    subject { chef_run }

    it do
      expect(
        chef_run.node['vault_tool']['config_file_path']
      ).to eql('/etc/vault/vault.json')
    end

    it do
      is_expected.to(
        create_packagecloud_repo('blessing_io/vault_tool')
          .with_type('rpm')
      )
    end
  end

  context 'debian with default attributes' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new(platform: 'debian', version: '7.6')
        .converge(described_recipe)
    end
    subject { chef_run }

    it do
      is_expected.to(
        create_packagecloud_repo('blessing_io/vault_tool')
          .with_type('deb')
      )
    end
  end
end
