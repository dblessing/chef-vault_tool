class Chef
  class Resource
    class VaultTool < Resource
      include Poise

      actions :init, :unseal, :seal, :mount, :unmount, :auth, :read, :write
      default_action :read

      attribute :bin_path,
                kind_of: String,
                default: lazy { node['vault_tool']['vault_bin_path'] }
      attribute :address, kind_of: String,  default: '127.0.0.1'
      attribute :port, kind_of: Integer, default: 8200
      attribute :tls,
                kind_of: [TrueClass, FalseClass],
                default: lazy { node['vault_tool']['client_tls'] }
      attribute :data_bag, kind_of: String, default: 'vault_tool'
      attribute :admins, kind_of: [String, Array]
      attribute :clients, kind_of: [String, Array]
      attribute :search, kind_of: String
      attribute :keys, kind_of: Hash
    end
  end

  class Provider
    class VaultTool < Provider
      require_relative 'helpers'
      begin
        require 'chef-vault'
      rescue LoadError
        Chef::Log.debug('Could not load chef-vault yet. It should be')
        Chef::Log.debug('available after running the chef-vault recipe.')
      end

      include Poise
      include Chef::Mixin::ShellOut
      include ::VaultTool::Helpers

      def action_init
        if status == :uninitialized
          converge_by "initialize vault #{new_resource.name}" do
            notifying_block do
              output = vault_shell_out!('init').stdout
              # parse output and grab the keys for chef vault
              data = parse_init_output(output)

              Chef::Log.debug("Creating chef vault with vault " \
                              "'#{new_resource.name}' encryption keys and root token")
              item = ChefVault::Item.new(new_resource.data_bag, data_bag_item)
              item.raw_data = item.raw_data.merge(data)
              item.admins([new_resource.admins].flatten.join(','))
              item.search(new_resource.search)
              item.clients(new_resource.clients)
              item.save
            end
          end
        end
      end

      def action_unseal
        if status == :sealed
          converge_by "unseal vault #{new_resource.name}" do
            notifying_block do
              Chef::Log.debug("Unseal threshold is #{threshold}")

              threshold.times do |i|
                result = vault_shell_out('unseal', encryption_keys["key#{i+1}"])
                unless result.stderr.empty?
                  Chef::Log.error(result.stderr.inspect)
                end
              end

              # Sanity check. The vault *should* be unsealed by now.
              # If not, something went terribly wrong.
              Chef::Log.error('Unseal was not successful') if status != :unsealed
            end
          end
        end
      end

      private

      def vault_shell_out!(cmd, sub_cmd = nil)
        shell_out!(compose_shell_cmd(cmd, sub_cmd))
      end

      def vault_shell_out(cmd, sub_cmd = nil)
        shell_out(compose_shell_cmd(cmd, sub_cmd))
      end

      def compose_shell_cmd(cmd, sub_cmd = nil)
        cmd = ["#{new_resource.bin_path} #{cmd}"]
        cmd << "#{vault_options}" if vault_options
        cmd << "#{sub_cmd}" if sub_cmd
        cmd.join(' ')
      end

      def vault_options
        opts = nil
        opts = "-address=http://#{new_resource.address}:#{new_resource.port}" unless new_resource.tls
        opts
      end

      def threshold
        status = vault_shell_out('status')
        status.stdout.match(/^Key\sThreshold:\s(\d)/)[1].to_i
      end

      def data_bag_item
        "#{new_resource.name}_encryption"
      end

      def encryption_keys
        @encryption_keys ||=
          if new_resource.keys
            Chef::Log.warn('Using keys from resource attribute. A chef vault is recommended')
            new_resource.keys
          else
            ChefVault::Item.load(new_resource.data_bag, data_bag_item)
          end
      end
    end
  end
end
