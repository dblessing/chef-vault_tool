module VaultTool
  module Helpers
    def status
      status = vault_shell_out('status')
      if !status.stderr.empty?
        status_from_stderr(status.stderr)
      else
        status_from_stdout(status.stdout)
      end
    end

    private

    def status_from_stderr(stderr)
      if stderr =~ Regexp.new('server is not yet initialized')
        Chef::Log.debug("Vault is unitialized")
        :uninitialized
      else
        Chef::Log.error("Vault status error: #{stderr}")
        nil
      end
    end

    def status_from_stdout(stdout)
      if stdout =~ Regexp.new('Sealed: true')
        Chef::Log.debug("Vault is sealed")
        :sealed
      elsif stdout =~ Regexp.new('Sealed: false')
        Chef::Log.debug("Vault is unsealed")
        :unsealed
      else
        Chef::Log.error("Vault status is unknown: #{stdout}")
        nil
      end 
    end

    def parse_init_output(output)
      data = {}
      output.split("\n").each do |row|
        if row =~ /^Key\s\d:/
          match = row.match(/^Key\s(?<id>\d):\s(?<data>.*)$/)
          data["key#{match[:id]}"] = match[:data]
        elsif row =~ /^Initial\sRoot\sToken:/
          match = row.match(/^Initial\sRoot\sToken:(?<token>.*)$/)
          data['root_token'] = match[:token]
        end
      end
      data
    end
  end
end
