name             'vault_tool'
maintainer       'Drew Blessing'
maintainer_email 'cookbooks@blessing.io'
license          'all_rights'
description      'Installs/Configures vault_tool'
long_description 'Installs/Configures vault_tool'
version          '0.1.0'

depends 'packagecloud', '~> 0.0'
depends 'runit', '~> 1.6'

depends 'poise', '~> 1.0'
depends 'chef-vault', '~> 1.2'
