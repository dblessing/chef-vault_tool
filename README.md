# DEFAULT README
This is the default README, DO NOT edit this README directly. Use knife-cookbook-doc plugin to generate
a README file. For information on generating a README file using the doc plugin,
[click here](doc/overview.md#generating-documentation)
